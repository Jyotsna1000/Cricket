import org.junit.jupiter.api.Test;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.*;

public class BatsmanTest {
    @Test
    void should_return_score_less_than_or_equal_to_six_when_bat() {
        Batsman batsman = new Batsman(BatsmanType.NORMAL);

        int runScored = batsman.bat();

        assertThat(runScored, is(lessThanOrEqualTo(6)));
    }

    @Test
    void should_return_boundaries_when_batsman_type_is_hitter() {
        Batsman batsman = new Batsman(BatsmanType.HITTER);

        int runScored = batsman.bat();

        assertThat(MatchConstant.boundaries, hasItem(runScored));
    }
}
