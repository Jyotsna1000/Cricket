import org.junit.jupiter.api.Test;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.is;
import static org.hamcrest.Matchers.lessThanOrEqualTo;

public class BowlerTest {
    @Test
    void should_return_score_less_than_or_equal_to_six_when_ball() {
        Bowler bowler = new Bowler();

        int runScored = bowler.ball();

        assertThat(runScored, is(lessThanOrEqualTo(6)));
    }
}
