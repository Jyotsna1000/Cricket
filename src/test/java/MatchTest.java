import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.Mockito;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.equalTo;
import static org.hamcrest.Matchers.is;
import static org.mockito.Mockito.when;

public class MatchTest {

    Batsman batsmanMock;
    Bowler bowlerMock;

    @BeforeEach
    void beforeEach() {
        batsmanMock = Mockito.mock(Batsman.class);
        bowlerMock = Mockito.mock(Bowler.class);
    }

    @Test
    void should_return_false_when_batsman_do_not_achieve_target() {
        Match match = new Match(25, 2, batsmanMock, bowlerMock);
        when(batsmanMock.bat()).thenReturn(2);
        when(bowlerMock.ball()).thenReturn(4);

        match.play();
        boolean actualResult = match.isTargetAchieved();

        assertThat(actualResult, is(equalTo(false)));
    }

    @Test
    void should_return_false_when_batsman_achieve_target() {
        Match match = new Match(23, 2, batsmanMock, bowlerMock);
        when(batsmanMock.bat()).thenReturn(2);
        when(bowlerMock.ball()).thenReturn(4);

        match.play();
        boolean actualResult = match.isTargetAchieved();

        assertThat(actualResult, is(equalTo(true)));
    }

    @Test
    void should_return_false_when_bowler_and_batsman_score_same() {
        Match match = new Match(24, 1, batsmanMock, bowlerMock);
        when(batsmanMock.bat()).thenReturn(4);
        when(bowlerMock.ball()).thenReturn(4);

        match.play();
        boolean actualResult = match.isTargetAchieved();

        assertThat(actualResult, is(equalTo(false)));
    }


    @Test
    void should_return_true_when_batsman_is_hitter() {
        Batsman batsman = new Batsman(BatsmanType.HITTER);
        Batsman hitterSpy = Mockito.spy(batsman);
        Match match = new Match(24, 1, hitterSpy, bowlerMock);
        when(hitterSpy.bat()).thenReturn(6);
        when(bowlerMock.ball()).thenReturn(4);

        match.play();
        boolean actualResult = match.isTargetAchieved();

        assertThat(actualResult, is(equalTo(true)));
    }
}
