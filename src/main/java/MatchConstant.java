import java.util.ArrayList;
import java.util.List;

public class MatchConstant {
    public static final int ballPerOver = 6;
    public static final int maxRunPerBall = 6;
    public static final List<Integer> boundaries =
            new ArrayList<>() {{
                add(0);
                add(4);
                add(6);
            }};
}
