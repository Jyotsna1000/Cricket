public class Batsman {
    private final BatsmanType batsmanType;

    public Batsman(BatsmanType batsmanType) {
        this.batsmanType = batsmanType;
    }

    public int bat() {
        if (batsmanType.equals(BatsmanType.HITTER)) {
            return MatchConstant.boundaries.get((int) (Math.random() * MatchConstant.boundaries.size()));
        } else {
            return (int) (Math.random() * MatchConstant.maxRunPerBall);
        }
    }
}
