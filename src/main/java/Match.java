public class Match {
    private final int target;
    private final int over;
    private final Batsman batsman;
    private final Bowler bowler;
    private int currentScore;

    public Match(int target, int over, Batsman batsman, Bowler bowler) {
        this.target = target;
        this.over = over;
        this.batsman = batsman;
        this.bowler = bowler;
    }

    public void play() {
        for (int counter = 0; counter < MatchConstant.ballPerOver * over; counter++) {
            int batsmanScore = batsman.bat();
            int bowlerScore = bowler.ball();
            if (isScoreOfBowlerAndBatsmanSame(bowlerScore, batsmanScore))
                break;
            currentScore += batsmanScore;
            if (isTargetAchieved())
                break;
        }
    }

    private boolean isScoreOfBowlerAndBatsmanSame(int bowlerScore, int batsmanScore) {
        return bowlerScore == batsmanScore;

    }

    public boolean isTargetAchieved() {
        return currentScore >= target;
    }
}
